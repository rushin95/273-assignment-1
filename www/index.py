from flask import Flask,request ,jsonify
from flask_sqlalchemy import SQLAlchemy
import json
#from datab import *
from flask_mysql import MySQL

#-----------------------------------------------------------------------------------------
from flask import Flask,request,jsonify, Response
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine
"""import json
import requests
import urllib2
import MySQLdb
import sys
from flask.ext.mysql import MySQL"""

app = Flask(__name__)
db = SQLAlchemy()
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://rushin:password@localhost:5000/ems'
app.config['SQL_TRACK_MODIFICATIONS'] = False
db.init_app(app)
engine = create_engine("mysql://rushin:password@localhost:5000/ems")

class Expensedetail(db.Model):

    __tablename__ = 'expenseDetail'
    id = db.Column(db.Integer, primary_key=True, nullable=False, autoincrement=True)
    username = db.Column(db.String(100))
    email = db.Column(db.String(30))
    category = db.Column(db.String(100))
    description = db.Column(db.String(100))
    link = db.Column(db.String(100))
    estimated_costs = db.Column(db.Integer)
    status = db.Column(db.String(100))
    submitted_date = db.Column(db.String(100))
    decision_date = db.Column(db.String(100))

    def __init__(self, username, email, category, description, link, estimated_costs, submit_date, status, decision_date):

        self.username = username
        self.email = email
        self.category = category
        self.description = description
        self.status = status
        self.link = link
        self.estimated_costs = estimated_costs
        self.status = status
        self.submit_date = submit_date

if not engine.dialect.has_table(engine, 'expenseDetail'):
    db.create_all()
#----------------------------------------------------------------------------------------------------

@app.route('/', methods=['GET'])
def welcome():
   return jsonify("Welcome to expense management system.")

@app.route('/v1/expenses', methods=['POST'])
def add_expense():
        name = request.data
        dataDict = json.loads(name)
        username = dataDict.get('name')
        email = dataDict.get('email')
        category = dataDict.get('category')
        description = dataDict.get('description')
        link = dataDict.get('link')
        estimated_costs = dataDict.get('estimated_costs')
        submit_date = dataDict.get('submit_date')
        newexpense = Expensedetail(username, email, category, description, link, estimated_costs,submit_date,"Pending","01-01-2016")


        db.session.add(newexpense)
        db.session.commit()
       # e=Expensedetail.query.filter_by(email='email').first()

        record ={'id': newexpense.id, 'name': username,'email': email,'category': category,'description':description,'link': link,'estimated_costs':estimated_costs,'submit_date':submit_date,'status': newexpense.status,'decision_date': newexpense.decision_date}
        return jsonify(record), 201

@app.route('/v1/expenses/<e_id>', methods=['GET'])
def get_expense(e_id):
       # response.get_json(force=True)
       expense=Expensedetail.query.filter_by(id=e_id).first()
       if expense is None:
                return "{}", 404
       else:
                record ={'id': expense.id, 'name': expense.username,'email': expense.email,'category': expense.category,'description': expense.description,'link': expense.link,'estimated_costs': str(expense.estimated_costs),'submit_date':expense.submitted_date,'status': expense.status,'decision_date': expense.decision_date}
                # resp = Response(status=200, mimetype='application/json')
                return json.dumps(record), 200

@app.route('/v1/expenses/<e_id>', methods=['PUT'])
def update_data(e_id):
        name = request.data
        dataDict = json.loads(name)
        new_cost=dataDict.get('estimated_costs')
        changed_exp=Expensedetail.query.get(e_id)
        changed_exp.estimated_costs=new_cost
        db.session.commit()
        return "{}", 202

@app.route('/v1/expenses/<e_id>', methods=['DELETE'])
def delete_expense(e_id):
        expense=Expensedetail.query.get(e_id)
        db.session.delete(expense)
        db.session.commit()
        return "{}", 204



if __name__ == "__main__":
        app.run(host="127.0.0.1", port=5000, debug="true")
